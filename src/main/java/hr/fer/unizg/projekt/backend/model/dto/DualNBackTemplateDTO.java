package hr.fer.unizg.projekt.backend.model.dto;

import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
//@AllArgsConstructor
//@NoArgsConstructor
public class DualNBackTemplateDTO {

    private String nameOfTemplate;
    private Long noOfTests;
    private Long noOfVerbalHits;
    private Long noOfSpatialHits;
    private Long timePerTest;
    private Long level;
    private Long noOfRounds;
    private String subjectId;

    public static DualNBackTemplateDTO of(DualNBackTemplate dualNBackTemplate) {
        return DualNBackTemplateDTO.builder()
                .level(dualNBackTemplate.getLevel())
                .nameOfTemplate(dualNBackTemplate.getNameOfTemplate())
                .noOfRounds(dualNBackTemplate.getNoOfRounds())
                .noOfSpatialHits(dualNBackTemplate.getNoOfSpatialHits())
                .noOfVerbalHits(dualNBackTemplate.getNoOfVerbalHits())
                .noOfTests(dualNBackTemplate.getNoOfTests())
                .subjectId(dualNBackTemplate.getSubjectId())
                .timePerTest(dualNBackTemplate.getTimePerTest())
                .build();
    }
}
