package hr.fer.unizg.projekt.backend.model.entity;

import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionTemplateDTO;
import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "COLLISION_DETECTION_TEMPLATE")
@EqualsAndHashCode(exclude = {"scores"})
public class CollisionDetectionTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", unique = true)
    private String nameOfTemplate;

    @Column(name = "MINIMUM_EDGE_DISTANCE")
    private Long minEdgeDistance;

    @Column(name = "MINIMUM_TIME_BETWEEN_COLLISIONS")
    private Long minTimeBetweenCollisions;

    @Column(name = "MAXIMUM_TIME_BETWEEN_COLLISIONS")
    private Long maxTimeBetweenCollisions;

    @Column(name = "MINIMUM_TIME_VISIBLE")
    private Long minTimeVisible;

    @Column(name = "POINT_RADIUS")
    private Long pointRadius;

    @Column(name = "MINIMUM_POINT_DISTANCE")
    private Long minimumPointDistance;

    @Column(name = "MAXIMUM_TIME_VISIBLE")
    private Long maxTimeVisible;

    @Column(name = "MAXIMUM_GAME_ELAPSED")
    private Long maxGameElapsed;

    @Column(name = "NO_OF_DUMMIES")
    private Long noOfDummies;

    @Column(name = "DISAPPEAR_AFTER")
    private Long disappearAfter;

    @Column(name = "RERENDER_RATE")
    private Long rerenderRate;

    @Column(name = "SEED")
    private String seed;

    @Column(name = "SUBJECT_ID")
    private String subjectId;

    @Column(name = "DELETED")
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User owner;

    @OneToMany(mappedBy = "template")
    private Set<DualNBackScore> scores;

    public CollisionDetectionTemplateDTO asDTO() {
        return CollisionDetectionTemplateDTO.builder()
                .nameOfTemplate(nameOfTemplate)
                .disappearAfter(disappearAfter)
                .maxGameElapsed(maxGameElapsed)
                .maxTimeBetweenCollisions(maxTimeBetweenCollisions)
                .maxTimeVisibleBeforeCollision(maxTimeVisible)
                .minEdgeDistance(minEdgeDistance)
                .minPointDistance(minimumPointDistance)
                .minTimeBetweenCollisions(minTimeBetweenCollisions)
                .minTimeVisibleBeforeCollision(minTimeVisible)
                .noOfDummies(noOfDummies)
                .pointRadius(pointRadius)
                .rerenderRate(rerenderRate)
                .seed(seed)
                .build();
    }

    public static CollisionDetectionTemplate of(CollisionDetectionTemplateDTO templateDTO,
                                                User user) {
        return builder()
                .disappearAfter(templateDTO.getDisappearAfter())
                .maxGameElapsed(templateDTO.getMaxGameElapsed())
                .maxTimeVisible(templateDTO.getMaxTimeVisibleBeforeCollision())
                .maxTimeBetweenCollisions(templateDTO.getMaxTimeBetweenCollisions())
                .isDeleted(false)
                .minEdgeDistance(templateDTO.getMinEdgeDistance())
                .minimumPointDistance(templateDTO.getMinPointDistance())
                .minTimeBetweenCollisions(templateDTO.getMinTimeBetweenCollisions())
                .minTimeVisible(templateDTO.getMinTimeVisibleBeforeCollision())
                .nameOfTemplate(templateDTO.getNameOfTemplate())
                .noOfDummies(templateDTO.getNoOfDummies())
                .owner(user)
                .scores(Collections.emptySet())
                .rerenderRate(templateDTO.getRerenderRate())
                .pointRadius(templateDTO.getPointRadius())
                .seed(templateDTO.getSeed())
                .build();
    }
}
