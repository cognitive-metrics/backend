package hr.fer.unizg.projekt.backend.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SHAPESHIFTER_SCORE")
public class ShapeShifterScore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private Byte[] score;

    @Column(name = "DONE")
    private Boolean isFinished;

    @ManyToOne
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID")
    private ShapeShifterTemplate template;
}
