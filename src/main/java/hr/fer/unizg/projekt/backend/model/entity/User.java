package hr.fer.unizg.projekt.backend.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USERS")
@EqualsAndHashCode(exclude = {"dualNBackTemplates", "shapeShifterTemplates"})
@ToString(exclude = {"dualNBackTemplates", "shapeShifterTemplates"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "EMAIL", unique = true, nullable = false)
    private String username;
    private String password;

    @OneToMany(mappedBy = "owner")
    private Set<DualNBackTemplate> dualNBackTemplates;

    @OneToMany(mappedBy = "owner")
    private Set<ShapeShifterTemplate> shapeShifterTemplates;

    @OneToMany(mappedBy = "owner")
    private Set<CollisionDetectionTemplate> collisionDetectionTemplates;

    public User(User other) {
        this.id = other.id;
        this.username = other.username;
        this.password = other.password;
        this.dualNBackTemplates = new HashSet<>(other.dualNBackTemplates);
        this.shapeShifterTemplates = new HashSet<>(other.shapeShifterTemplates);
    }
}
