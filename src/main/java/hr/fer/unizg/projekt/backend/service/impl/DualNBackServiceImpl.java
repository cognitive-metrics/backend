package hr.fer.unizg.projekt.backend.service.impl;

import hr.fer.unizg.projekt.backend.exception.NoSuchGameFoundException;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.DualNBackScore;
import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;
import hr.fer.unizg.projekt.backend.repository.DualNBackScoreRepository;
import hr.fer.unizg.projekt.backend.repository.DualNBackTemplateRepository;
import hr.fer.unizg.projekt.backend.service.DualNBackService;
import hr.fer.unizg.projekt.backend.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Transactional
public class DualNBackServiceImpl implements DualNBackService {

    private final DualNBackTemplateRepository dualNBackTemplateRepository;
    private final DualNBackScoreRepository dualNBackScoreRepository;
    private final UserService userService;

    @Override
    @PreAuthorize(value = "hasRole('USER')")
    public DualNBackTemplate saveTemplate(DualNBackTemplateDTO dualNBackTemplateDTO) {
        return dualNBackTemplateRepository.save(
                DualNBackTemplate.of(
                        dualNBackTemplateDTO,
                        userService.loggedInUser()));
    }

    @Override
    public Long createTest(DualNBackTemplateDTO dualNBackTemplateDTO) {
        DualNBackTemplate template;
        if (dualNBackTemplateDTO.getNameOfTemplate() != null
                && !dualNBackTemplateDTO.getNameOfTemplate().isEmpty()) {
            template = dualNBackTemplateRepository.findFirstByNameOfTemplate(dualNBackTemplateDTO.getNameOfTemplate())
                    .orElseGet(() -> dualNBackTemplateRepository.save(
                            DualNBackTemplate.of(
                                    dualNBackTemplateDTO,
                                    userService.loggedInUser())));
        } else {
            DualNBackTemplateDTO newTemplate = dualNBackTemplateDTO.toBuilder()
                    .nameOfTemplate(null)
                    .build();
            template = dualNBackTemplateRepository.save(
                    DualNBackTemplate.of(
                            newTemplate,
                            userService.loggedInUser()));
        }
        return dualNBackScoreRepository.save(
                DualNBackScore.builder()
                        .template(template)
                        .isFinished(Boolean.FALSE)
                        .build())
                .getId();
    }

    @Override
    public DualNBackTemplate getTest(Long gameID) {
        return dualNBackScoreRepository.findById(gameID)
                .orElseThrow(() -> new NoSuchGameFoundException("No game with ID: " + gameID + " found"))
                .getTemplate();
    }

    @SneakyThrows
    @Override
    public void saveScore(DualNBackScoreDTO dualNBackScoreDTO,
                          Long gameID) {
        DualNBackScore dualNBackScore = dualNBackScoreRepository.findById(gameID)
                .orElseThrow(() -> new NoSuchGameFoundException("No game with ID: " + gameID + " found"));
        if (dualNBackScore.getIsFinished()) {
            throw new NoSuchGameFoundException("Test has been taken already");
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        new ObjectOutputStream(byteArrayOutputStream)
                .writeObject(dualNBackScoreDTO.getResult());
        dualNBackScore.setScore(byteArrayOutputStream.toByteArray());
        dualNBackScore.setIsFinished(Boolean.TRUE);
        dualNBackScoreRepository.save(dualNBackScore);
    }

    @Override
    public void deleteScore(Long id) {
        dualNBackScoreRepository.deleteById(id);
    }

    @Override
    @PreAuthorize(value = "hasRole('USER')")
    public Set<DualNBackTemplate> getTemplates() {
        return userService.loggedInUser()
                .getDualNBackTemplates();
    }

    @Override
    public void deleteTemplate(String name) {
        dualNBackTemplateRepository.findFirstByNameOfTemplate(name)
                .ifPresent(template -> {
                    template.setIsDeleted(true);
                    dualNBackTemplateRepository.save(template);
                });
    }
}
