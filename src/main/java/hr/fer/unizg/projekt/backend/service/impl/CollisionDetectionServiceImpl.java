package hr.fer.unizg.projekt.backend.service.impl;

import hr.fer.unizg.projekt.backend.exception.NoSuchGameFoundException;
import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionTemplate;
import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionScore;
import hr.fer.unizg.projekt.backend.repository.CollisionDetectionScoreRepository;
import hr.fer.unizg.projekt.backend.repository.CollisionDetectionTemplateRepository;
import hr.fer.unizg.projekt.backend.service.CollisionDetectionService;
import hr.fer.unizg.projekt.backend.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Transactional
public class CollisionDetectionServiceImpl implements CollisionDetectionService {

    private final CollisionDetectionTemplateRepository collisionDetectionTemplateRepository;
    private final CollisionDetectionScoreRepository collisionDetectionScoreRepository;
    private final UserService userService;

    @Override
    @PreAuthorize(value = "hasRole('USER')")
    public CollisionDetectionTemplate saveTemplate(CollisionDetectionTemplateDTO collisionDetectionTemplateDTO) {
        return collisionDetectionTemplateRepository.save(
                CollisionDetectionTemplate.of(
                        collisionDetectionTemplateDTO,
                        userService.loggedInUser()));
    }

    @Override
    public Long createTest(CollisionDetectionTemplateDTO collisionDetectionTemplateDTO) {
        CollisionDetectionTemplate template;
        if (collisionDetectionTemplateDTO.getNameOfTemplate() != null
                && !collisionDetectionTemplateDTO.getNameOfTemplate().isEmpty()) {
            template = collisionDetectionTemplateRepository.findFirstByNameOfTemplate(collisionDetectionTemplateDTO.getNameOfTemplate())
                    .orElseGet(() -> collisionDetectionTemplateRepository.save(
                            CollisionDetectionTemplate.of(
                                    collisionDetectionTemplateDTO,
                                    userService.loggedInUser())));
        } else {
            CollisionDetectionTemplateDTO newTemplate = collisionDetectionTemplateDTO.toBuilder()
                    .nameOfTemplate(null)
                    .build();
            template = collisionDetectionTemplateRepository.save(
                    CollisionDetectionTemplate.of(
                            newTemplate,
                            userService.loggedInUser()));
        }
        return collisionDetectionScoreRepository.save(
                CollisionDetectionScore.builder()
                        .template(template)
                        .isFinished(Boolean.FALSE)
                        .build())
                .getId();
    }

    @Override
    public CollisionDetectionTemplate getTest(Long gameID) {
        return collisionDetectionScoreRepository.findById(gameID)
                .orElseThrow(() -> new NoSuchGameFoundException("No game with ID: " + gameID + " found"))
                .getTemplate();
    }

    @SneakyThrows
    @Override
    public void saveScore(CollisionDetectionScoreDTO collisionDetectionScoreDTO,
                          Long gameID) {
        CollisionDetectionScore collisionDetectionScore = collisionDetectionScoreRepository.findById(gameID)
                .orElseThrow(() -> new NoSuchGameFoundException("No game with ID: " + gameID + " found"));
        if (collisionDetectionScore.getIsFinished()) {
            throw new NoSuchGameFoundException("Test has been taken already");
        }
        collisionDetectionScore.setHits(collisionDetectionScoreDTO.getHits());
        collisionDetectionScore.setWrongSelections(collisionDetectionScoreDTO.getWrongs());
        collisionDetectionScore.setMisses(collisionDetectionScoreDTO.getMisses());
        collisionDetectionScore.setIsFinished(Boolean.TRUE);
        collisionDetectionScoreRepository.save(collisionDetectionScore);
    }

    @Override
    public void deleteScore(Long id) {
        collisionDetectionScoreRepository.deleteById(id);
    }

    @Override
    @PreAuthorize(value = "hasRole('USER')")
    public Set<CollisionDetectionTemplate> getTemplates() {
        return userService.loggedInUser()
                .getCollisionDetectionTemplates();
    }

    @Override
    public void deleteTemplate(String name) {
        collisionDetectionTemplateRepository.findFirstByNameOfTemplate(name)
                .ifPresent(template -> {
                    template.setIsDeleted(true);
                    collisionDetectionTemplateRepository.save(template);
                });
    }

}
