package hr.fer.unizg.projekt.backend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class BasicAuthController {

    @GetMapping("login")
    public ResponseEntity<String> login() {
        return ResponseEntity.ok().build();
    }
}
