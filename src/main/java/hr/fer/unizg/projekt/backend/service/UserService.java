package hr.fer.unizg.projekt.backend.service;

import hr.fer.unizg.projekt.backend.model.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User loggedInUser();
}
