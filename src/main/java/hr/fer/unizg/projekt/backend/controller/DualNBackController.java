package hr.fer.unizg.projekt.backend.controller;

import hr.fer.unizg.projekt.backend.model.dto.DualNBackScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;
import hr.fer.unizg.projekt.backend.service.DualNBackService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("dual-n-back")
@RequiredArgsConstructor
@CrossOrigin("*")
public class DualNBackController {

    private final DualNBackService dualNBackService;

    @Value("${domain}")
    private String domain;

    @PostMapping
    public String generateTest(@RequestBody DualNBackTemplateDTO dualNBackTemplateDTO) {
        return domain + "/dual-n-back/" + dualNBackService.createTest(dualNBackTemplateDTO);
    }

    @GetMapping("{gameID}")
    public DualNBackTemplateDTO getTest(@PathVariable("gameID") Long gameID) {
        return dualNBackService.getTest(gameID)
                .asDTO();
    }

    @PostMapping("{gameID}")
    public ResponseEntity<String> saveScore(@PathVariable("gameID") Long gameID,
                                            @RequestBody DualNBackScoreDTO dualNBackScore) {
        dualNBackService.saveScore(dualNBackScore, gameID);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{gameID}")
    public ResponseEntity<String> deleteScore(@PathVariable("gameID") Long gameID) {
        dualNBackService.deleteScore(gameID);
        return ResponseEntity.ok().build();
    }

    @PostMapping("template")
    public DualNBackTemplateDTO createTemplate(@RequestBody DualNBackTemplateDTO dualNBackTemplateDTO) {
        return dualNBackService.saveTemplate(dualNBackTemplateDTO)
                .asDTO();
    }

    @GetMapping("template")
    public Set<DualNBackTemplateDTO> getTemplates() {
        return dualNBackService.getTemplates()
                .stream()
                .filter(template -> template.getNameOfTemplate() != null)
                .filter(template -> !template.getIsDeleted())
                .map(DualNBackTemplate::asDTO)
                .collect(Collectors.toSet());
    }

    @DeleteMapping("template/{name}")
    public Set<DualNBackTemplateDTO> deleteTemplate(@PathVariable("name") String templateName) {
        dualNBackService.deleteTemplate(templateName);
        return dualNBackService.getTemplates()
                .stream()
                .filter(template -> template.getNameOfTemplate() != null)
                .filter(template -> !template.getIsDeleted())
                .map(DualNBackTemplate::asDTO)
                .collect(Collectors.toSet());
    }
}
