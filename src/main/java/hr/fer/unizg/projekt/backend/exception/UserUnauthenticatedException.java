package hr.fer.unizg.projekt.backend.exception;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
@NoArgsConstructor
public class UserUnauthenticatedException extends RuntimeException {

    public UserUnauthenticatedException(String msg) {
        super(msg);
    }
}
