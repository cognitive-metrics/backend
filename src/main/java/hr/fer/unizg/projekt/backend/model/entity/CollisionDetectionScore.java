package hr.fer.unizg.projekt.backend.model.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionScoreDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@Table(name = "COLISSION_DETECTION_SCORE")
public class CollisionDetectionScore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DONE")
    @Builder.Default
    private Boolean isFinished = Boolean.FALSE;

    @Column(name = "HITS", columnDefinition = "integer[]")
    @Type(type = "list-array")
    private List<Long> hits;

    @Column(name = "MISSES")
    private Long misses;

    @Column(name = "WRONG_SELECTIONS")
    private Long wrongSelections;

    @ManyToOne
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID")
    private CollisionDetectionTemplate template;

    public static CollisionDetectionScore of(CollisionDetectionScoreDTO scoreDTO) {
        return builder()
                .hits(scoreDTO.getHits())
                .misses(scoreDTO.getMisses())
                .wrongSelections(scoreDTO.getWrongs())
                .build();
    }
}
