package hr.fer.unizg.projekt.backend.config;

import hr.fer.unizg.projekt.backend.model.entity.User;
import hr.fer.unizg.projekt.backend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class Initialize {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Bean
    public void handleContextStart() {
        if (!userRepository.findUserByUsername("a@f.g").isPresent()) {
            System.out.println("Creating user");
            userRepository.save(User.builder()
                    .username("a@f.g")
                    .password(passwordEncoder.encode("password"))
                    .build());
        }
    }
}
