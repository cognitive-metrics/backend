package hr.fer.unizg.projekt.backend.service;

import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionTemplate;

import java.util.Set;

public interface CollisionDetectionService {

    CollisionDetectionTemplate saveTemplate(CollisionDetectionTemplateDTO collisionDetectionTemplateDTO);

    Long createTest(CollisionDetectionTemplateDTO collisionDetectionTemplateDTO);

    CollisionDetectionTemplate getTest(Long id);

    void saveScore(CollisionDetectionScoreDTO collisionDetectionScoreDTO,
                   Long gameID);

    void deleteScore(Long id);

    Set<CollisionDetectionTemplate> getTemplates();

    void deleteTemplate(String name);

}
