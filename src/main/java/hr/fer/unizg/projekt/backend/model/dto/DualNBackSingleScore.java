package hr.fer.unizg.projekt.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.io.Serializable;

@Value
@AllArgsConstructor
//@NoArgsConstructor
public class DualNBackSingleScore implements Serializable {
    private Long spatialMisses;
    private Long verbalMisses;
}
