package hr.fer.unizg.projekt.backend.service;

import hr.fer.unizg.projekt.backend.model.dto.DualNBackScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;

import java.util.Set;

public interface DualNBackService {

    DualNBackTemplate saveTemplate(DualNBackTemplateDTO dualNBackTemplateDTO);

    Long createTest(DualNBackTemplateDTO dualNBackTemplateDTO);

    DualNBackTemplate getTest(Long id);

    void saveScore(DualNBackScoreDTO dualNBackScoreDTO,
                   Long gameID);

    void deleteScore(Long id);

    Set<DualNBackTemplate> getTemplates();

    void deleteTemplate(String name);
}
