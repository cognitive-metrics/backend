package hr.fer.unizg.projekt.backend.model.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder(toBuilder = true)
public class CollisionDetectionScoreDTO {

  List<Long> hits;
  Long misses;
  Long wrongs;

}
