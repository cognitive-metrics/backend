package hr.fer.unizg.projekt.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DualNBackScoreDTO {
    private List<DualNBackSingleScore> result;
}
