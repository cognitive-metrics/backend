package hr.fer.unizg.projekt.backend.repository;

import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollisionDetectionScoreRepository extends JpaRepository<CollisionDetectionScore, Long> {
}
