package hr.fer.unizg.projekt.backend.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SHAPESHIFTER_TEMPLATE")
public class ShapeShifterTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", unique = true, nullable = true)
    private String nameOfTemplate;

    @Column(name = "TESTS")
    private Long noOfTests;

    @Column(name = "MIN_TRANSITIONS")
    private Long minNoOfTransitions;

    @Column(name = "MAX_TRANSITIONS")
    private Long maxNoOfTransitions;

    @Column(name = "TIME")
    private Long timePerTest;

    @Column(name = "VELOCITY")
    private Long velocity;

    @Column(name = "ROUNDS")
    private Long noOfRounds;

    @Column(name = "SUBJECT")
    private String subjectId;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User owner;

    @OneToMany(mappedBy = "template")
    private Set<ShapeShifterScore> scores;
}
