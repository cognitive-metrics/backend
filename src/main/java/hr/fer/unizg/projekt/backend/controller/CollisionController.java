package hr.fer.unizg.projekt.backend.controller;

import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.CollisionDetectionTemplateDTO;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackScoreDTO;
import hr.fer.unizg.projekt.backend.model.dto.DualNBackTemplateDTO;
import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionTemplate;
import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;
import hr.fer.unizg.projekt.backend.service.CollisionDetectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("collisions")
@RequiredArgsConstructor
@CrossOrigin("*")
public class CollisionController {

    private final CollisionDetectionService collisionDetectionService;

    @Value("${domain}")
    private String domain;

    @PostMapping
    public String generateTest(@RequestBody CollisionDetectionTemplateDTO collisionDetectionTemplateDTO) {
        return domain + "?id=" + collisionDetectionService.createTest(collisionDetectionTemplateDTO);
    }

    @GetMapping("{gameID}")
    public CollisionDetectionTemplateDTO getTest(@PathVariable("gameID") Long gameID) {
        return collisionDetectionService.getTest(gameID)
                .asDTO();
    }

    @PostMapping("{gameID}")
    public ResponseEntity<String> saveScore(@PathVariable("gameID") Long gameID,
                                            @RequestBody CollisionDetectionScoreDTO collisionDetectionScoreDTO) {
        collisionDetectionService.saveScore(collisionDetectionScoreDTO, gameID);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{gameID}")
    public ResponseEntity<String> deleteScore(@PathVariable("gameID") Long gameID) {
        collisionDetectionService.deleteScore(gameID);
        return ResponseEntity.ok().build();
    }

    @PostMapping("template")
    public CollisionDetectionTemplateDTO createTemplate(@RequestBody CollisionDetectionTemplateDTO collisionDetectionTemplateDTO) {
        return collisionDetectionService.saveTemplate(collisionDetectionTemplateDTO)
                .asDTO();
    }

    @GetMapping("template")
    public Set<CollisionDetectionTemplateDTO> getTemplates() {
        return collisionDetectionService.getTemplates()
                .stream()
                .filter(template -> template.getNameOfTemplate() != null)
                .filter(template -> !template.getIsDeleted())
                .map(CollisionDetectionTemplate::asDTO)
                .collect(Collectors.toSet());
    }

    @DeleteMapping("template/{name}")
    public Set<CollisionDetectionTemplateDTO> deleteTemplate(@PathVariable("name") String templateName) {
        collisionDetectionService.deleteTemplate(templateName);
        return collisionDetectionService.getTemplates()
                .stream()
                .filter(template -> template.getNameOfTemplate() != null)
                .filter(template -> !template.getIsDeleted())
                .map(CollisionDetectionTemplate::asDTO)
                .collect(Collectors.toSet());
    }
}
