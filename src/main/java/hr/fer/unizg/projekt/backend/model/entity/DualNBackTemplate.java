package hr.fer.unizg.projekt.backend.model.entity;

import hr.fer.unizg.projekt.backend.model.dto.DualNBackTemplateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collections;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DUAL_N_BACK_TEMPLATE")
@EqualsAndHashCode(exclude = {"scores"})
public class DualNBackTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", unique = true)
    private String nameOfTemplate;

    @Column(name = "TESTS")
    private Long noOfTests;

    @Column(name = "VERBAL_HITS")
    private Long noOfVerbalHits;

    @Column(name = "SPATIAL_HITS")
    private Long noOfSpatialHits;

    @Column(name = "TIME")
    private Long timePerTest;

    @Column(name = "LEVEL")
    private Long level;

    @Column(name = "ROUNDS")
    private Long noOfRounds;

    @Column(name = "SUBJECT")
    private String subjectId;

    @Column(name = "DELETED")
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User owner;

    @OneToMany(mappedBy = "template")
    private Set<DualNBackScore> scores;

    public static DualNBackTemplate of(DualNBackTemplateDTO dualNBackTemplateDTO,
                                       User user) {
        return DualNBackTemplate.builder()
                .isDeleted(false)
                .level(dualNBackTemplateDTO.getLevel())
                .nameOfTemplate(dualNBackTemplateDTO.getNameOfTemplate())
                .noOfRounds(dualNBackTemplateDTO.getNoOfRounds())
                .noOfSpatialHits(dualNBackTemplateDTO.getNoOfSpatialHits())
                .noOfVerbalHits(dualNBackTemplateDTO.getNoOfVerbalHits())
                .noOfTests(dualNBackTemplateDTO.getNoOfTests())
                .owner(user)
                .subjectId(dualNBackTemplateDTO.getSubjectId())
                .timePerTest(dualNBackTemplateDTO.getTimePerTest())
                .scores(Collections.emptySet())
                .build();
    }

    public DualNBackTemplateDTO asDTO() {
        return DualNBackTemplateDTO.of(this);
    }

}
