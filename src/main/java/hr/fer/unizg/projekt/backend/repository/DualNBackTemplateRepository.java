package hr.fer.unizg.projekt.backend.repository;

import hr.fer.unizg.projekt.backend.model.entity.DualNBackTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DualNBackTemplateRepository extends JpaRepository<DualNBackTemplate, Long> {

    void deleteByNameOfTemplate(String name);

    Optional<DualNBackTemplate> findFirstByNameOfTemplate(String name);
}
