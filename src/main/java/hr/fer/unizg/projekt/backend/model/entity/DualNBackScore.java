package hr.fer.unizg.projekt.backend.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DUAL_N_BACK_SCORE")
public class DualNBackScore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private byte[] score;

    @Column(name = "DONE")
    @Builder.Default
    private Boolean isFinished = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID")
    private DualNBackTemplate template;

}
