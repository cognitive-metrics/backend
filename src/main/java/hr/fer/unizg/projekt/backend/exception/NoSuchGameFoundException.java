package hr.fer.unizg.projekt.backend.exception;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoSuchGameFoundException extends RuntimeException {

    public NoSuchGameFoundException(String msg) {
        super(msg);
    }
}
