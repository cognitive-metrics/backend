package hr.fer.unizg.projekt.backend.repository;

import hr.fer.unizg.projekt.backend.model.entity.CollisionDetectionTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CollisionDetectionTemplateRepository extends JpaRepository<CollisionDetectionTemplate, Long> {

    void deleteByNameOfTemplate(String name);

    Optional<CollisionDetectionTemplate> findFirstByNameOfTemplate(String name);
}
