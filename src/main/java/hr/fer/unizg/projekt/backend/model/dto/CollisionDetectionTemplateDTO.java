package hr.fer.unizg.projekt.backend.model.dto;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class CollisionDetectionTemplateDTO {

    String nameOfTemplate;
    Long minEdgeDistance;
    Long minTimeBetweenCollisions;
    Long maxTimeBetweenCollisions;
    Long minTimeVisibleBeforeCollision;
    Long pointRadius;
    Long minPointDistance;
    Long maxTimeVisibleBeforeCollision;
    Long maxGameElapsed;
    Long noOfDummies;
    Long disappearAfter;
    Long rerenderRate;
    String seed;
    String subjectId;

}
